﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace NPC_Dialog
{
    /*Zadání na cvičení - Dialogový systém
        Navrhněte a vytvořte vhodnou datovou strukturu pro dialogový systém hry.

        1, Každé dialogové okno se skládá s textu od NPC a několika možností pro hráče. 
        2, Poté, co si hráč vybere jednu z možností, následuje další dialogové okno nebo konec dialogu. 
        3, Každá z reakcí může být zakázána na základě stavu vnějších proměnných 
            (skilly, staty, stav inventáře, dokončené kvesty, žijí potřebná NPC? atd.)
        4,Váš dialogový systém bude obsahovat metody, 
            které umožňují pohyb po povolených větvých dialogu a metody pro uložení a načtení dialogu 
            (volba datového formátu je na vás).*/
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();

            Dictionary<int, Sentence> dialog = new Dictionary<int, Sentence>()
            {
                {
                    0 , new Sentence("Ahoj, jak je ?",
                        new Answer("Dobre", 1),
                        new Answer("Spatne", 1)
                        )
                },
                {
                    1, new Sentence("Chces si vybrat ?",
                        new Answer("Ne", -1),
                        new Answer("Ano", -1)
                        )
                }
            };

            while (game.Running)
            {
                Console.Write(game.Introduction());
                CheckForExit(game);

                Console.WriteLine(dialog[0].ToString());
                dialog[0].Odpovedi();
                Console.Write("Odpoved: ");
                string input = Console.ReadLine();
                if(input == "1")
                {
                    Console.WriteLine(dialog[1].ToString());
                    dialog[1].Odpovedi();
                    input = Console.ReadLine();
                    if (input == "1")
                    {
                        Console.WriteLine("Ahaaaa ");
                    }
                }
            }
        }

        private static void CheckForExit(Game game)
        {
            string input = Console.ReadLine();
            input.ToLower();
            if (input == "n" || input == "ne")
            {
                game.Running = false;
                System.Environment.Exit(1);
            }
        }
    }
}
