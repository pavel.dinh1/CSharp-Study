﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC_Dialog
{
    class Game
    {
        private bool bRunning = true;
        public bool Running { get { return bRunning; } set { bRunning = value; } }

        public Game()
        {

        }

        public string Introduction()
        {
            return "Vitej u obchodnika se zbranemi.\n" +
                "Prejes si nakoupit nejake zbozi ? \n" +
                "Stiskni libovolnou klavesu pro ANO, nebo -> Ne/N - ne/n pro ukonceni. \n" +
                "Odpoved: ";
        }
    }
}
