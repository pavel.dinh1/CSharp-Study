﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC_Dialog
{
    class Sentence
    {
        IEnumerable<Answer> answers;
        public string sentence;
        public Sentence(string sent, params Answer[] answrs)
        {
            this.sentence = sent;
            this.answers = answrs;
        }

        public void Odpovedi()
        {
            foreach(Answer an in answers)
            {
                Console.WriteLine(an.ToString());
            }
        }

        public override string ToString()
        {
            return sentence;
        }
    }
}
