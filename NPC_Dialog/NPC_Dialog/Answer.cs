﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NPC_Dialog
{
    class Answer
    {
        private string answer;
        private int answerIndex;
        public Answer(string answer, int index)
        {
            this.answer = answer;
            answerIndex = index;
        }

        public override string ToString()
        {
            return answer;
        }
    }
}
